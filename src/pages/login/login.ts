import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';



import { IUser } from '../../interfaces/IUser';

import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { AngularFireAuth } from 'angularfire2/auth';
import { PreferencesProvider } from '../../providers/preferences/preferences';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as IUser;
  photoPerfil: string = '../../assets/imgs/photo_perfil.png';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public fire: AngularFireAuth,
    public preferences: PreferencesProvider,) {

  }

  login(user) {
    user.photo = 'https://upload.wikimedia.org/wikipedia/commons/7/72/Default-welcomer.png'
    if (user.email == null || user.password == null) {
      this.alert('Erro', 'Prencha os campos email e senha.')
    } else {
      this.auth.login(user).then((res) => {
        this.preferences.create(user)
        this.navCtrl.setRoot(HomePage);
      }).catch((erro) => {
        if (erro.code == 'auth/invalid-email') {
          this.alert('Erro', 'O endereço de email é inválido')
        } else if (erro.code == 'auth/user-disabled') {
          this.alert('Erro', ' Email fornecido desativado.')
        } else if (erro.code == 'auth/user-not-found') {
          this.alert('Erro', 'Não existe usúario correspodente ao email fornecido.')
        } else if (erro.code == 'auth/wrong-password') {
          this.alert('Erro', 'Email ou senha inválidos')
        }
      })
    }
  }

  loginWithFace(){
    this.auth.loginWithFacebook().then((res) => {
      this.navCtrl.setRoot(HomePage);
    }).catch((erro: any)=>{
      this.alert(erro.code, erro.message)
      this.navCtrl.setRoot(HomePage)
    })
  }

  loginHowAnonimo(){
    this.auth.loginAnonimo().then((data) => {
      console.log('loginHowAnonimo', data);
      this.navCtrl.setRoot(HomePage);
    }).catch((erro: any) => {
      this.alert(erro.code,erro.message)
    })
  }

  registerPage(){

  }

  recoverPage(){

  }

  alert(title, subTitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['OK']
    });
    alert.present();
  }

  toast(message){
    let toast = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'dark-trans',
      closeButtonText: 'OK',
      showCloseButton: true
    });
    toast.setMessage(message)
    toast.present();

  }

  //alert fica na posição top, solução editar alert-top do main.css para padding: 0px e  align-items: center
  forgotPass() {
    let alert = this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: "Digite o endereço do seu email para enviarmos o link de redefinição de senha.",
      cssClass: 'custom-alert',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          role: 'cancel',
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: data => {
             this.fire.auth.sendPasswordResetEmail(data.email).then((success)=>{
               console.log('success', success)
                this.toast('Email enviado com sucesso')
             }).catch((err)=>{
               console.log('falha',err.code, err.message)
               this.toast('Falha ao enviar, verifique seus dados')
             })
          }
        }
      ]
    });
    alert.present();
  }


}
