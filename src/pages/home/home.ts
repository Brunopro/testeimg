import { Component } from '@angular/core';
import { NavController, ToastController, UrlSerializer, AlertController, LoadingController } from 'ionic-angular';
import { ImagePicker } from '@ionic-native/image-picker';

import { normalizeURL } from 'ionic-angular';

import { Crop } from '@ionic-native/crop';

import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { IUser } from '../../interfaces/IUser';
import { PreferencesProvider } from '../../providers/preferences/preferences';

import { Diagnostic } from '@ionic-native/diagnostic';

import { Camera, CameraOptions } from '@ionic-native/camera'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public opcoesDeCamera: CameraOptions = {
    allowEdit: true,
    cameraDirection: 0,
    correctOrientation: false,
    destinationType: 0,
    encodingType: 1,
    quality: 90,
    saveToPhotoAlbum: true,
    sourceType: 1,
    targetHeight: 800,
    targetWidth: 800
  }

  image: string = '';

  user: IUser

  show_content: boolean = true;

  loading: boolean = false;

  constructor(public navCtrl: NavController,
    private imagePicker: ImagePicker,
    private toastCtrl: ToastController,
    private cropService: Crop,
    private preferences: PreferencesProvider,
    private af: AngularFireAuth,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private camera: Camera) {
    preferences.get().then((data) => { this.user = data; console.log(data.photo) })
  }
  
  onTakePicture() {
    this.opcoesDeCamera.sourceType = 1;
    this.camera.getPicture(this.opcoesDeCamera).then((imageData) => {
      this.image = 'data:image/png;base64,' + imageData;
      }, (err) => {
        this.displayErrorAlert(err);
      });
  }

  displayErrorAlert(err){
    console.log(err);
    let alert = this.alertCtrl.create({
       title: 'Error',
       subTitle: 'Error while trying to capture picture',
       buttons: ['OK']
     });
     alert.present();
}

  /*
  penImagePicker() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if (result == true) {
          console.log('executando1')
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  */

  /*
  //primeiro passo
  openImagePickerCrop() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (!result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission().then(() => {
            this.getImage(result)
          }).catch((err) => {
            console.log(err)
          })
        } else {
          this.getImage(result);
        }
      }, (err) => {
        console.log(err);
      }).catch( err => console.error(err))
  }
  */

  openImagePickerCrop() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.confirm()
        }
        else if (result == true) {
          this.getImage()
        }
      }, (err) => {
        console.log(err);
      });
  }





  getImage() {
    this.imagePicker.getPictures({
      maximumImagesCount: 1
    }).then(
      (results) => {
        for (var i = 0; i < results.length; i++) {
          this.cropService.crop(results[i], { quality: 75 }).then(
            newImage => {
              this.loading = true;
              this.uploadImageToFirebase(newImage);
            },
            error => console.error("Error cropping image", error)
          );
        }
      }, (err) => console.log(err)
    ).catch(
      err => console.error(err)
    )
  }

  //segundo passo
  uploadImageToFirebase(image) {
    console.log('executando2')
    image = normalizeURL(image);

    //uploads img to firebase storage
    this.uploadImage(image)
      .then(photoURL => {

        this.preferences.get().then((user) => {
          user.photo = photoURL;
          this.preferences.create(user).then(() => this.preferences.get().then((data) => { this.user = data; console.log(data.photo); this.loading = false }))
        })

        console.log('photoURL', photoURL);

        let toast = this.toastCtrl.create({
          message: 'Image was updated successfully',
          duration: 3000
        });
        toast.present();
      })
  }

  //terceiro passo
  uploadImage(imageURI) {

    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(this.af.auth.currentUser.uid);//salvando imagem com nome do uid do user
      this.encodeImageUri(imageURI, function (image64) {

        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            //buscando link da imagem no storage
            snapshot.ref.getDownloadURL()
              .then(downloadUrl => {
                //devolvendo link da imagem para function uploadImageToFirebase
                resolve(downloadUrl)
              })
              .catch(err => {
                console.log(err);
                //catch error here
                reject(err);
              });
          }, err => {
            console.log(err)
            reject(err);
          })
      })
    })
  }


  //quarto passo
  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };

  confirm() {
    let alert = this.alertCtrl.create({
      message: 'O celular protegido não tem permissão para acessar as suas fotos. Para liberar o acesso, click em OK!',
      cssClass: 'myalert-confirm myalert-header',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.diagnostic.requestExternalStorageAuthorization().then((status) => {
              console.log("Authorization request for external storage use was " + (status == this.diagnostic.permissionStatus.GRANTED ? "granted" : "denied"));
              if (status == this.diagnostic.permissionStatus.GRANTED) {
                this.getImage()
              }
            }).catch((err) => console.error('err requestExternalStorageAuthorization', err))
          }
        }
      ]
    });
    alert.present();
  }



}

